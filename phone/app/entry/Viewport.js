﻿/**
 * 程序入口类
 */
Ext.define("Entry.Viewport", {
    extend: "Ext.Container",
    xtype: 'entryview',
    id: 'entryview',
    config: {
        layout: 'card',
        autoDestroy:false,
            items: [{
				xtype:'cardview',
			},{
                xtype: 'navigationpanel',
				cls:'footbar',
				docked:'bottom'
            }]
    }
});