/**
 * card布局文件
 */
Ext.define("Entry.Card", {
    extend: "Ext.Container",
    xtype: 'cardview',
    id: 'cardview',
    config: {
        layout: 'card',
        autoDestroy:false,
		items: [{
			xtype:'mainpanel'
		},{
			xtype:'callpanel'
		},{
			xtype:'contactspanel'
		},{
			xtype:'dialpanel'
		}]
	}
});