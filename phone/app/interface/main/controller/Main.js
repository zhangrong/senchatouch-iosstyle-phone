﻿/**
 * main的控制层，用以实现对view层组件的操作
 */
Ext.define('Main.controller.Main', {
    extend: 'Ext.app.Controller',
	config: {
        refs:{
            entryView: '#entryview',
			//上面是引用card布局的总组件
			//下面是表示当前组件，亲们在创建的所有组件都可以用这种形式调用，
			//未注释的和注释的分别是2种引用方式，有些自定义的界面组件没法引入后发现内容为空就可以尝试第二种方式
            mainView: 'mainpanel'
			/* mainView:{
                xtype: 'mainpanel',
                selector: 'mainpanel',
                autoCreate: true
            }, */
        },
		//下面是组件的方法
        control: {
			'mainpanel':{
				//show:'showFn'
			},
			'mainpanel list':{
				itemtap : 'itemtapFn'
			},
			'maindialog':{
				initialize : 'initFn'
			}
		}
    },
	/**
	 * 因为mainpanel组件继承自container所以拥有show方法
	 * 这个方法的意思就是当mainpanel显示出来的时候执行这个方法
	 */
	showFn:function(cmp){
		Ext.Msg.alert('我show出来了，但是我现在不准备做什么操作，但是我很有用');
	},
	/**
	 * 列表点击事件
	 * 关于列表的点击事件，我想着重啰嗦一下，很多亲希望在list有很多不同的操作，
	 * 但是itemtap事件只有一个，为了能实现某些特定的操作，有些甚至用上了onItemDisclosure，
	 * 额，关于这个onItemDisclosure，我其实不知道干什么的，但是说句心里话，我看到st自带的那个列表右侧的图标很不爽
	 * 其实，只要亲们足够细心，和去找资料，对于列表的操作（基本的操作）一个itemtap完全可以搞定，
	 * 关于参数，我就不多解释了，去看API就了解了，多种操作的重点在于第五个参数event
	 */
	itemtapFn:function(list,index,target,record,event){
		var me = this,
			main = me.getMainView(),
			eTag = event.target.className;//你点击到的标签的class
			//比方说我在list里面写了一个span，它里面有一个class="firstName"
		if(eTag == "firstName"){
			Ext.Viewport.add({
				xtype: 'maindialog',
				context:record
			});
		}else{
			
		}
		//依次类推，如果你用浏览器的调试模式看event.target的话，你就会用的非常利索
	},
	initFn:function(cmp){
		var record = cmp.getContext(),
			content = cmp.down('#content');
			content.setHtml(record.get('content'));
	}
});