﻿/**
 *main的store，用于获取main需要的store数据
 * 这里只是简单的应用，详细的亲务必看API
 *
 */
Ext.define('Main.store.Main',{
	extend : 'Ext.data.Store',
	config : {
		storeId:'mainStore',//这个用于标识store，唯一的
		autoLoad:true,//这个是重点，意思是允许自动加载，如果没写这句，就会默认false,就必须手动加载，手动加载的方法本章后面给出
		model : 'Main.model.Main',//数据以这个model的关键字的显示到项目中
		pageSize:10,
		proxy : {
			type : 'ajax',//另外一种格式就是jsonp,jsonp我通常在访问数据接口会用到，ajax在访问本地数据用的比较多，关于这些，要深入理解去好好研究下API
			url : 'person.json'//这里是文件名，如果是服务器接口，这里写接口的url
		}
		//下面是传参方法
		//,params:{}//参数，因为我这里没有参数，所以我注释掉了，关于API怎么用，下面图我已经给出,每一个属性都可以在这里加上，如果你的项目需要用到,
		//下面是排序方法
		/* , sorters:{
			property:'name',//按name字段进行排序
			direction:'DESC'//或者DESC,排序是按字母表的顺序哦，要按数字的，请看API
		}  */
		//下面是分组方法
		//
		/* ,grouper: {
				groupFn: function(record) {
						return record.get('name').substr(0, 1);
					},
				sortProperty: 'name'
			}*/
	} 
		//这些东西API里面已经说的很详细了，不会用多看API
});
