﻿/**
 *  main的model，用于定义模块中store中出现的项目需要的关键字
 */
Ext.define('Main.model.Main', {
    extend: 'Ext.data.Model',
    config: {
        fields: [ 'name','address', 'number']
		//这里面最简单，当然，如果要处理里面的关键字的信息，就没那么简单，不过前期先这样，
		//这里定义的是store中的关键字，也是项目中要用到的关键字段
		//API提供了一些其他操作，不过前期先学会使用就可
    }
});