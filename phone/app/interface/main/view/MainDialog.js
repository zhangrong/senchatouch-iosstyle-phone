﻿/**
 * 导位界面
 */
Ext.define('Main.view.MainDialog', {
    extend: 'Ext.Container',
    xtype: 'maindialog',
    config: {
        context: [], //存放
        layout:  'fit',
		centered:true,
		modal:true,
        hideOnMaskTap: true,
		style:'border:1px red solid;',
        width: 300,
        height:250,
        items: [
            {
                xtype: 'toolbar',
				height:40,
				docked:'top',
                title:'许伟是猪'
            },{
				xtype:'container',
				itemId:'content',
				html:''
			}
        ]
    }
});