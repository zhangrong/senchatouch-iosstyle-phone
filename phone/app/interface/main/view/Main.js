﻿/**
 * main的view层，用以定义界面的组件
 */
Ext.define('Main.view.Main',{
    extend:'Ext.Container',
	xtype:'mainpanel',
	config:{
		layout:'fit',//fit布局，list显示必用布局否则要list显示就要设置高度
		items:[{
			xtype:'toolbar',
			title:'个人收藏',
			docked:'top',//toolbar 固定在顶部不受fit控制
			items:[{
				xtype:'spacer'//站位spacer，用在这里的只是为了站位，效果就是下面iconbutton靠在tb的右边
			},{
				iconCls:'add'
			}]
		},{
			xtype:'list',//这里我特意留下我的样式
			//grouped: true,分组，写完store中的分组属性，别忘了在此处加上这个哦
			//{..}中的关键字就是你在model中定义的关键字
			itemTpl: ['<tpl><div>',
					  '<span class="firstName" style="margin: 1px 20px 5px 0;padding:7px 10px;border: 1px #ccc solid;border-radius: 50px;background: #ccc;">{[values.name.substring(0,1).toUpperCase()]}</span>',
					  '<span>{name}</span>',
					  '<span style="float:right;">{address}&nbsp;&nbsp;<span style="padding:1px 8px;border: 1px #347CE9 solid;border-radius:15px;color: #347CE9;">!<span></span>',
					  '</div></tpl>'
					  ],
			store:'mainStore',//引用storeId:mainStore的store,请看main/store/Main.js中
		}]
	}
});
