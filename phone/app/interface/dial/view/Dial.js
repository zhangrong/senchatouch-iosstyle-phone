﻿Ext.define('Dial.view.Dial',{
    extend:'Ext.Container',
	xtype:'dialpanel',
	config:{
		layout:'fit',
		items:[{
			xtype:'container',
			docked:'top',
			height:80,
		},{
			xtype : 'container',
			centered:true,
			defaults: {
				margin:10,
				defaults: {
					flex: 1,
					margin:10,
					height:60,
					width:60
				}   
			},
			items:[{
				xtype: 'panel',
				layout: 'hbox',
				items:[
					{xtype:'button',html:'<div><span style="font-size: 2em;">1</span></div>'},
					{xtype:'button',html:'<div><span style="font-size: 2em;">2</span><span style="font-size: .5em;position: absolute;bottom: 12px;left: 32px;">ABC</span></div>'},
					{xtype:'button',html:'<div><span style="font-size: 2em;">3</span><span style="font-size: .5em;position: absolute;bottom: 12px;left: 32px;">DEF</span></div>'}
				]
			},{
				xtype: 'panel',
				layout: 'hbox',
				items:[
					{xtype:'button',html:'<div><span style="font-size: 2em;">4</span><span style="font-size: .5em;position: absolute;bottom: 12px;left: 32px;">GHI</span></div>'},
					{xtype:'button',html:'<div><span style="font-size: 2em;">5</span><span style="font-size: .5em;position: absolute;bottom: 12px;left: 32px;">JKL</span></div>'},
					{xtype:'button',html:'<div><span style="font-size: 2em;">6</span><span style="font-size: .5em;position: absolute;bottom: 12px;left: 32px;">MNO</span></div>'}
				]
			},{
				xtype: 'panel',
				layout: 'hbox',
				items:[
					{xtype:'button',html:'<div><span style="font-size: 2em;">7</span><span style="font-size: .5em;position: absolute;bottom: 12px;left: 30px;">PQRS</span></div>'},

					{xtype:'button',html:'<div><span style="font-size: 2em;">8</span><span style="font-size: .5em;position: absolute;bottom: 12px;left: 32px;">TUV</span></div>'},

					{xtype:'button',html:'<div><span style="font-size: 2em;">9</span><span style="font-size: .5em;position: absolute;bottom: 12px;left: 30px;">WXYZ</span></div>'}
				]
			},{
				xtype: 'panel',
				layout: 'hbox',
				items:[
					{xtype:'button',html:'<div><span style="font-size: 1.7em;">※</span></div>'},

					{xtype:'button',html:'<div><span style="font-size: 2em;">0</span><span style="font-size: 1.2em;position: absolute;bottom: 12px;left:39px;">+</span></div>'},

					{xtype:'button',html:'<div><span style="font-size: 1.7em;">#</span></div>'}
				]
			}]
		},{
				xtype:'container',
				docked:'bottom',
				height:80,
				layout: {
					type: 'hbox',
					align: 'middle ',
					pack: 'center'
				},
				items:[{
					xtype:'button',
					cls:'call_title_segment',
					margin:10,
					width:330,
					text:'呼叫',
					action:'call'
				}]
		}]
	}
});
