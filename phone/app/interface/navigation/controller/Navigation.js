﻿Ext.define('Navigation.controller.Navigation', {
    extend: 'Ext.app.Controller',
    config: {
        refs: {
            cardView: '#cardview',
            mainView: 'mainpanel',
			navigationView:'navigationpanel',
        },
        control: {
            'navigationpanel segmentedbutton': {
                toggle: 'toggleForCategoryFn'
            }
        }
    },
	/**
	 * 分类导航的方法
	 */
	toggleForCategoryFn:function(cmp, button, pressed){
		var me = this,
			cardView = me.getCardView();
        if(button.getText() ==="个人收藏"&&pressed ===true){
			cardView.setActiveItem(0);
		}else if(button.getText() ==="最近通话"&&pressed ===true){
			cardView.setActiveItem(1);
		}else if(button.getText() ==="通讯录"&&pressed ===true){
			cardView.setActiveItem(2);
		}else if(button.getText() ==="拨号按键"&&pressed ===true){
			cardView.setActiveItem(3);
		}else if(button.getText() ==="语音留言"&&pressed ===true){
			cardView.setActiveItem(4);
		}
	}
});