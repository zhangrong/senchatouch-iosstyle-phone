﻿Ext.define('Call.view.Call',{
    extend:'Ext.Container',
	xtype:'callpanel',
	config:{
		layout:'fit',
		items:[{
			xtype:'toolbar',
			docked:'top',
			items:[{
				xtype:'segmentedbutton',
				cls:'call_title_segment',
				centered:true,
				items: [{
							text: '所有通话',
							pressed: true
						},{
							text: '最近通话'
						}
					],
				},{
					xtype:'spacer'
				},{
				xtype:'button',
				text:'编辑',
			}]
		},{
			xtype:'list'
		}]
	}
});
