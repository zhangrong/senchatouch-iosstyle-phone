/**
 * 项目之初需要配置一下加载的类
 */
//这一块原默认创建的项目中是没有的
Ext.Loader.setConfig({
    enabled:true ,//激活
	//这里是配置类的目录。
    paths:{
        'Entry':'app/entry' , // 系统入口
		'Main':'app/interface/main',  // 第一个主页面
		'Navigation':'app/interface/navigation',//底部导航栏界面
		'Call':'app/interface/call',//通话记录界面
		'Contacts':'app/interface/contacts',//通讯录界面
		'Dial':'app/interface/dial',//拨号界面
	}
});

Ext.application({
    name: 'phone',//项目名

	//你所用到的组件
    requires: [
        'Ext.MessageBox','Entry.Card','Ext.SegmentedButton',
		'Ext.dataview.List'
    ],
	
	//你创建的所有的视图
    views: [
        'Entry.Viewport',//入口界面，因为是整个界面的集合，所以是view
		'Main.view.Main',//main界面，以后你在项目中添加的每一个界面都必须出现在这里。后面不再备注
		'Main.view.MainDialog',
		'Navigation.view.Navigation',
		'Call.view.Call',
		'Contacts.view.Contacts',
		'Dial.view.Dial'
	],
	//你创建的所有的controller
	controllers:['Main.controller.Main','Navigation.controller.Navigation'],
	
	//你所创建的所有的store
	stores:['Main.store.Main'],
	
	//你所创建的所有的model
	models:['Main.model.Main'],
	//图标
    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },
	//运行图标优先显示
    isIconPrecomposed: true,
	//不同分辨率对应的启动图片
    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    launch: function() {
        // 销毁加载图片
        Ext.fly('appLoadingIndicator').destroy();

        // 初始化主要界面，这里是你项目的运行入口
        Ext.Viewport.add({
			//官方默认的是这个入口，因为我们知道那个类是入口的。没必要这么用
			//Ext.create('phone.entry.Main')
				xtype:'entryview'
			});
    },
	//需要更新的时候操作
    onUpdated: function() {
        Ext.Msg.confirm(
            "系统提示",
            "加载新版本完成，是否重新启动？",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
